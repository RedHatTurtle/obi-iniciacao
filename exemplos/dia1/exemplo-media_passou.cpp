// programa para calcular a média de três notas
#include <iostream>

int p1,p2,p3;
float media;

int main() {
  // Lê as três notas
  scanf("%d %d %d", &p1, &p2, &p3);
  // calcula
  media = (p1+p2+p3)/3.0;
  printf("Média: %f\n",media);
  // imprime
  if (media >= 5.0) {
    printf("Aprovado\n");
  }
  else {
    printf("Recuperação\n");
  }
  return 0;
}
