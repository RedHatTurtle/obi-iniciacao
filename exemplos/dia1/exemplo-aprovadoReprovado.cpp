// Programa para calcular a media de tres notas
#include <iostream>

int main() {
    int p1, p2, p3;
    float media;

    // Le as tres notas
    scanf("%d %d %d", &p1, &p2, &p3);

    // Calcula
    media = (p1 + p2 + p3) / 3.0;
    printf("Media: %f\n", media);

    // Imprime
    if (media >= 5.0) {
        printf("Aprovado\n");
    }
    else {
        printf("Recuperacao\n");
    }

    return 0;
}
