#include <iostream>

int N,maior,vice,nota,i;

int main() {

  scanf("%d",&N);
  // vamos inicializar maior e vice com as duas primeiras notas
  scanf("%d %d",&maior,&nota);
  if (nota > maior) {
    vice = maior;
    maior = nota;
  }
  else
    vice = nota;
  // agora vamos ler as outras notas
  i = 2;
  while (i<N) {
    scanf("%d",&nota);
    if (nota > maior) {
      vice = maior;
      maior = nota;
    }
    else if (nota > vice) {
      vice = nota;
    }
    i++;
  }
  printf("%d\n",vice);
  return 0;
}
