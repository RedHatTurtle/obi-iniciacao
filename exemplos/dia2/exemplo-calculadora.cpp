// programa Calculadora
#include <iostream>

int x,y;
char op;

int main() {
  // Lê a expressão
  scanf("%d%c%d", &x, &op, &y);

  if (op=='+')      // note que se bloco tem um comando chaves não são necessárias
    printf("%d\n",x+y);
  else if (op=='-') 
    printf("%d\n",x-y);
  else if (op=='*') 
    printf("%d\n",x*y);
  else if (op=='/') 
    printf("%d\n",x/y);

  return 0;
}
