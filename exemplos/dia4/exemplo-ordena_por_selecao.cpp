#include <iostream>

int main() {

    int N;
    int vet[100];
    int pos, j, tmp, pos_menor;

    // Lê a entrada
    scanf("%d", &N);
    for (pos=0; pos<N; pos++)
        scanf("%d", &vet[pos]);

    // Ordenar por seleção
    for (pos=0; pos<N-1; pos++) {
        pos_menor = pos;

        // Encontrar a posição do menor númro
        for (j=pos+1 ; j<N ; j++) {
            if (vet[j] < vet[pos_menor])
                pos_menor = j;
        }
        printf("Posição do menor número: %d\n", pos_menor);

        // Se a posição do menor não é a posição correta, trocar eles de lugar
        if (pos_menor != pos) {
            printf("Trocando de lugares\n");
            tmp = vet[pos];
            vet[pos] = vet[pos_menor];
            vet[pos_menor] = tmp;
        }

        printf("Nova ordem: ");
        for (int i=0; i<N; i++)
            printf("%d ", vet[i]);
        printf("\n\n");
    }

    // Imprimir o vetor ordenado
    for (pos=0; pos<N; pos++)
        printf("%d ", vet[pos]);
    printf("\n");

  return 0;
}
