#include <iostream>

int vet[100];
int i,N;

int main() {
  // lê o vetor
  scanf("%d", &N);
  for (i=0; i<N; i++)
    scanf("%d", &vet[i]);

  // imprime invertido
  for (i=N-1; i>=0; i--)
    printf("%d ",vet[i]);
  printf("\n");

  return 0;
}
