#include <iostream>

int N,i,atual,anterior;

int main() {

  // lê número de valores
  scanf("%d", &N);

  // inicializa anterior
  anterior = -1;
  for (i=0; i<N; i++) {
    // lê mais um valor "atual"
    scanf("%d", &atual);
    if (atual < anterior)
      break;
    anterior = atual;
  }

  if (atual < anterior)
    printf("N\n");
  else
    printf("S\n");
  return 0;
}
