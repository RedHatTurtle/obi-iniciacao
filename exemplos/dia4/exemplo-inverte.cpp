#include <iostream>

int vet[100];
int i,j,N,tmp;

int main() {

  // lê entrada
  scanf("%d", &N);
  for (i=0; i<N; i++)
    scanf("%d", &vet[i]);

  //inverte o vetor
  i=0;
  j=N-1;
  while (i<j) {
    tmp = vet[i];
    vet[i] = vet[j];
    vet[j] = tmp;
    i++; j--;
  }
  
  // imprime o vetor invertido
  for (i=0; i<N; i++)
    printf("%d ", vet[i]);
  printf("\n");

  return 0;
}
