#include <iostream>

int main() {
    int N, i, atual, anterior;
    anterior = -1;

    scanf("%d", &N);

    for(i=0 ; i<N ; i++){
        scanf("%d", &atual);

        if (atual > anterior)
        {
            anterior = atual;
            if (i == N-1)
            {
                printf("S\n");
            }
        }
        else
        {
            printf("N\n");
            break;
        }
    }

    return 0;
}
