#include <iostream>

int main() {
    int N, i, atual, anterior;
    int vet[100];
    anterior = -1;

    scanf("%d", &N);

    for(i=0 ; i<N ; i++)
        scanf("%d", &vet[i]);

    for (int j = 0 ; j<N ; j++)
    {
        for(i=1 ; i<N ; i++){

            anterior = vet[i-1];
            atual = vet[i];

            if (atual < anterior)
            {
                vet[i-1] = atual;
                vet[i]   = anterior;
            }
        }
    }

    for(i=0 ; i<N ; i++)
        printf("%d ", vet[i]);
    printf("\n");

    return 0;
}
