#include <iostream>

int main() {
    int jogo[3][3];
    int i, p, win=2;
    char vencedor='N';

    for (i=0; i<3; i++){
        for (p=0; p<3; p++){
            scanf("%d", &jogo[i][p]);
        }
    }

    for(p=0;p<2;p++){ // Loop pelos jogadores
        for(i=0;i<3;i++){ // Loop pelas linas/colunas
            if(jogo[i][0]==p && jogo[i][1]==p && jogo[i][2]==p){
                win=p;
            }
            if(jogo[0][i]==p && jogo[1][i]==p && jogo[2][i]==p){
                win=p;
            }
        }

        if(jogo[0][0]==p && jogo[1][1]==p && jogo[2][2]==p)
            win=p;
        if(jogo[2][0]==p && jogo[1][1]==p && jogo[0][2]==p)
            win=p;
    }

    if (win==0) vencedor = 'O';
    if (win==1) vencedor = 'X';
    if (win==2) vencedor = 'V';

    printf("%c\n", vencedor);
    return 0;
}
