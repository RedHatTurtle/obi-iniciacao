#include <iostream>

int main() {
  int freq[11]; // Um elemento para cada nota de 0 a 10
  int i, j, N, nota;

  scanf("%d", &N);

  // Inicializa vetor de frequência
  for (i=0; i<11; i++)
    freq[i] = 0;

  // Lê cada nota e atualiza o elemento correspondente no vetor frequência
  for (i=0; i<N; i++) {
    scanf("%d", &nota);
    freq[nota]++; // abreviatura de freq[nota] = freq[nota] + 1
  }

  // Imprime o resultado
  for (i=0; i<11; i++) {
    printf("%d ", i);

    for (j=0; j<freq[i]; j++)
      printf("*");

    printf("\n");
  }
  return 0;
}

