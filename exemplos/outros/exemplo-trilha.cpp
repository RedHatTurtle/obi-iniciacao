#include <iostream>

int main(){
    int atual, custo, custoDescida, custoSubida, i, num_trilha, j, min, n, anterior, pontos;

	scanf("%d",&n);
	min = -1;

	for (i = 0; i < n; i++) {
		scanf("%d", &pontos);
		anterior = atual = custoSubida = custoDescida = 0;

		for (j = 0; j < pontos; j++){
			anterior = atual;
			scanf("%d", &atual);
			if (j != 0) {
				if(atual > anterior)
				    custoSubida += (atual - anterior);
				else if(atual < anterior)
				    custoDescida += (anterior - atual);
			}
		}

		if (custoDescida < custoSubida) {
            custo = custoDescida;
		}
		else {
            custo = custoSubida;
		}

		if (min > custo) {
		    min = custo;
		    num_trilha = i + 1;
		}
	}

	printf("%d\n", num_trilha);

	return 0;
}
