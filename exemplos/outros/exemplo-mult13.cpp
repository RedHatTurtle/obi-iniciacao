#include <iostream>

int main() {
    int a, b, i, sum=0;

    scanf("%d %d", &a, &b);

    if(b<a){
        i=a;
        a=b;
        b=i;
    }

    for(i=a;i<=b;i++){
        if (i%13!=0){
            sum+=i;
        }
    }

    printf("%d\n", sum);

    return 0;
}
