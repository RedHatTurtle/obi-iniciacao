#include <iostream>

int main() {
    int repetir=1, cont=0;
    float nota, sum=0;

    while(repetir==1) {
        while(cont<2) {

            scanf("%f", &nota);

            if (nota >= 0 && nota <= 10){
                sum+=nota;
                cont++;
            }else{ //nota invalida
                printf("nota invalida\n");
            }
        }

        printf("media = %.2f\n", sum/2.0);

        repetir=0;
        while (repetir != 1 && repetir != 2) {
            printf("novo calculo (1-sim 2-nao)\n");
            scanf("%d", &repetir);
        }

        cont=0;
        sum=0;
    }

    return 0;
}
