#include <iostream>

int main() {
  float sal, imp = 0.0;

  scanf("%f\n", &sal);

  if (sal <= 2000.0){
    printf("Isento\n");
    return 0;
  }else if ( sal <= 3000.0 ){
    imp = 0.08*(sal-2000.0);
    printf("R$ %.2f\n", imp);
    return 0;
  }else if ( sal <= 4500.0 ){
    imp = 80.0+0.18*(sal-3000.0);
    printf("R$ %.2f\n", imp);
    return 0;
  }else{
    imp = 350.0+0.28*(sal-4500.0);
    printf("R$ %.2f\n", imp);
    return 0;
  }
}
