#include <iostream>

int main() {
    float nota, sum=0;
    int repetir=1, cont=0;

    while(repetir==1){
        while(cont<2){

            scanf("%f", &nota);

            if (nota >= 0 && nota <= 10){
                sum+=nota;
                cont++;
            }else{ //nota invalida
                printf("nota invalida\n");
            }
        }//while(cont<2)
        printf("media = %.2f\n", sum/2.0);

        repetir=0;
        while (repetir != 1 && repetir != 2){
            printf("novo calculo (1-sim 2-nao)\n");
            scanf("%d", &repetir);
        }//while (repetir != 1 && repetir != 2)

        cont=0;
        sum=0;
    }//while(repetir==1)

    return 0;
}
