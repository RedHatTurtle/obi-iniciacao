#include <iostream>

int main () {
    int n;
    int nota, minimo=101;

    scanf("%d", &n);

    for ( n=n ; n>0  ; n=n-1 ) {
        scanf("%d", &nota);
        if ( nota < minimo) {
            minimo = nota;
        }
    }

    printf("%d", minimo);

    return 0;
}
