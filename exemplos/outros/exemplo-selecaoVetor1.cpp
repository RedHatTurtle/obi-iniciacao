#include <iostream>

int main() {
    int i;
    float vec[100];

    for(i=0;i<100;i++)
        scanf("%f", &vec[i]);

    for(i=0;i<100;i++){
        if (vec[i] <= 10)
            printf("A[%d] = %.1f\n", i, vec[i]);
    }

    return 0;
}
