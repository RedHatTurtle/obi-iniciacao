#include <iostream>

int main() {
    int linhas, colunas;
    int mapa[100][100];
    int i, j, movimentos;

  // Lê o número de linhas e o número de colunas do mapa
  scanf("%d %d", &linhas, &colunas);

  // Cria uma moldura para o mapa
  // Molduras horizontais
  for (j=0; j<colunas+2; j++) {
    mapa[0][j] = -1;
    mapa[linhas+1][j] = -1;
  }
  // Molduras verticais
  for (i=0; i<linhas+2; i++) {
    mapa[i][0] = -1;
    mapa[i][colunas+1] = -1;
  }

  // Lê o mapa
  for (i=0; i<linhas; i++)
    for (j=0; j<colunas; j++)
      scanf("%d", &mapa[i][j]);

  // Inicia da posicao 1 1
  movimentos = 0;
  i=1; j=1;

  // Andando
  while (true) {
    mapa[i][j] = 2; // Marca essa posição como visitada

    // Procura uma posição válida e anda para ela
    if (mapa[i-1][j]==1) {
      i = i-1; // Anda para baixo
    }
    else if (mapa[i+1][j]==1) {
      i = i+1; // Anda para cima
    }
    else if (mapa[i][j-1]==1) {
      j = j-1; // Anda para a esquerda
    }
    else if (mapa[i][j+1]==1) {
      j = j+1; // Anda para a direita
    }
    else {
      // Se nenhuma das posições ao redor for válida então terminamos e podemos sair do loop
      break;
    }

    // Conta um movimento a mais
    movimentos++;
  }

  // Imprime o resultado
  printf("%d\n", movimentos);
  return 0;
}
