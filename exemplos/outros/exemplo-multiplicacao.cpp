#include <iostream>

int main() {
    int m, n, i, j, k, soma = 0;

    scanf("%d %d", &m, &n);

    int m1[m][n], m2[n][m], m3[m][m];

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
          scanf("%d", &m1[i][j]);

    for (i = 0; i < n; i++)
        for (j = 0; j < m; j++)
          scanf("%d", &m2[i][j]);

    for (i = 0; i < m; i++) {
      for (j = 0; j < m; j++) {
        for (k = 0; k < n; k++) {
          soma = soma + m1[i][k] * m2[k][j];
        }

        m3[i][j] = soma;
        soma = 0;
      }
    }

    for (i = 0; i < m; i++) {
      for (j = 0; j < m; j++)
        printf("%d ", m3[i][j]);

      printf("\n");
    }

  return 0;
}
