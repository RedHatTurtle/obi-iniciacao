#include <iostream>

int vet[100],pos,j,tmp,N,pos_menor;

int main() {

  // Lê a entrada
  scanf("%d", &N);
  for (pos=0; pos<N; pos++)
    scanf("%d", &vet[pos]);

  // ordena
  for (pos=0; pos<N-1; pos++) {
    pos_menor = pos;
    for (j=pos+1; j<N; j++) {
      if (vet[j] < vet[pos_menor])
	pos_menor = j;
    }
    if (pos_menor != pos) {
      tmp = vet[pos];
      vet[pos] = vet[pos_menor];
      vet[pos_menor] = tmp;
    }
  }
  // imprime o vetor ordenado
  for (pos=0; pos<N; pos++)
    printf("%d ", vet[pos]);
  printf("\n");

  return 0;
}


