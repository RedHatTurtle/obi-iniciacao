#include <iostream>

int main() {
    int numAlunos, contador;
    float nota, somaNotas;
    float mediaTurma;

    scanf("%d", &numAlunos);

    contador = 0;
    somaNotas = 0;

    while( contador < numAlunos ){
        scanf("%f", &nota);

        somaNotas = somaNotas + nota;
        // Forma alternativa:
        //somaNotas += nota;

        contador = contador + 1;
        // Forma alternativa:
        // contador++;
    }

    mediaTurma = somaNotas / numAlunos;
    printf("A média da turma é: %.2f\n", mediaTurma);

    return 0;
}

// Ex de entrada/saída:
//4
//9.4
//3.5
//4.7
//5.7
//A média da turma é: 5.82
