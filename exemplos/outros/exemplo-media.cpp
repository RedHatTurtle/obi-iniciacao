// programa para calcular a m�dia de tr�s notas
#include <iostream>

int p1,p2,p3;
float media;

int main() {
  // L� as tr�s notas
  scanf("%d %d %d", &p1, &p2, &p3);

  // calcula e imprime o resultado
  media = (p1+p2+p3)/3;
  printf("M�dia: %f\n",media);

  return 0;
}
