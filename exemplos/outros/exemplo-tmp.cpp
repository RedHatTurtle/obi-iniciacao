#include<iostream>

int acima,soma,N,i;
float media;
int notas[100];

int main() {
  scanf("%d",&N);

  soma = 0;
  for (i=0; i<N; i++) {
    scanf("%d",&notas[i]);
    soma = soma + notas[i];
  }
  
  // calcula a média
  media = soma/N;

  // agora conta quantos estão acima da média
  acima = 0;
  for (i=0; i<N; i++) {
    if (notas[i] > media)
      acima++;
  }

  // imprime o resultado
  printf("%f %d\n",media,acima);
}
