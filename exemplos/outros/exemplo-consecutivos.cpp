#include <iostream>

int main() {
    int n, consecutivos, max=0, x1, x2, i;

    scanf("%d", &n);

    scanf("%d", &x1);
    consecutivos = 1;

    for(i=1 ; i<n ; i++){
        scanf("%d", &x2);
        if (x2==x1)
            consecutivos++;
        else
            consecutivos=1;
        if (consecutivos>max)
            max = consecutivos;
        x1=x2;
    }

    printf("%d\n", max);

    return 0;
}
