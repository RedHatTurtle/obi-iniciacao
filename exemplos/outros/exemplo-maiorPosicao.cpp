#include <iostream>

int main() {
    int nBandejas, latas, copos, quebrados, i;

    scanf("%d", &nBandejas);
    quebrados = 0;

    for(i=1;i<=nBandejas;i++){
        scanf("%d %d", &latas, &copos);
        if (latas>copos){
            quebrados += copos;
        }
    }

    printf("%d\n", quebrados);

    return 0;
}
