#include <iostream>

int main() {
    int n, aux;
    int vet[100];
    int procura, comeco, i;

    scanf("%d", &n);

    //Ler vetor
    for (i=0 ; i<n ; i++){
        scanf("%d", &vet[i]);
    }

    //Ordenar a sequencia
    comeco=0;
    for (procura=0 ; procura<=1000000 ; procura++){
        for (i=comeco ; i<n ; i++){
            if (vet[i]==procura){
                aux = vet[comeco];
                vet[comeco] = vet[i];
                vet[i] = aux;
                comeco++;
                break;
            }
        }
    }

    //Escrever vetor
    for (i=0 ; i<n ; i++){
        printf("%d ", vet[i]);
    }

    return 0;
}
