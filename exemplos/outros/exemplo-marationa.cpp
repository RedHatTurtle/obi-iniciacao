#include <iostream>

int main() {
    int n, dist, x, xold, i;
    char res='S';

    scanf("%d %d", &n, &dist);

    scanf("%d", &xold);
    for(i=1;i<=n;i++){
        scanf("%d", &x);
        if (x-xold>dist){
            res = 'N';
        }
        xold = x;
    }
    if (42195-xold>dist)
        res = 'N';

    printf("%c\n", res);

    return 0;
}
