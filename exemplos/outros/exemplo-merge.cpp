#include <iostream>

int main() {
    int tamM, tamN;
    int M[100], N[100];
    int i, j;

    scanf("%d %d", &tamM, &tamN);

    for(i=0 ; i<tamM ; i++){
        scanf("%d", &M[i]);
    }

    for(j=0 ; j<tamN ; j++){
        scanf("%d", &N[j]);
    }

    i=0;
    j=0;
    while(i+j<tamM+tamN){
        if(M[i]<N[j]){
            printf("%d ", M[i]);
            i++;
        }else{
            printf("%d ", N[j]);
            j++;
        }
        if(i>=tamM){
            for(j=j ; j<tamN ; j++)
                printf("%d ", N[j]);
            break;
        }
        if(j>=tamN){
            for(i=i ; i<tamM ; i++)
                printf("%d ", M[i]);
            break;
        }
    }
    printf("\n");

    return 0;
}
