#include <iostream>

int main () {
    int m, n;
    int i, j, k;
    int vet1[100], vet2[100];

    scanf ("%d %d", &m, &n);

    for ( i=0 ; i<m ; i++ ){
        scanf("%d", &vet1[i]);
    }

    for ( i=0 ; i<n ; i++ ){
        scanf("%d", &vet2[i]);
    }

    j=0;
    k=0;
    for(i=1 ; i<=m+n ; i++){
        if (j<m && k<n){
            if (vet1[j]<=vet2[k]){
                printf("%d ", vet1[j]);
                j++;
            } else {
                printf("%d ", vet2[k]);
                k++;
            }
        } else if (j==m){
            printf("%d ", vet2[k]);
            k++;
        } else if (k==n){
            printf("%d ", vet1[j]);
            j++;
        }
    }

    return 0;
}
