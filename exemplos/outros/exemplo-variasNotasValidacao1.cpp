#include <iostream>

int main() {
    int repete=1, cont = 0;
    float nota;
    float notas[2];

    do {
        cont = 0;
        while(cont < 2) {
            scanf("%f", &nota);
            if(nota >=0 && nota <=10) {
                notas[cont] = nota;
                cont++;
            }
            else
                printf("nota invalida\n");
        }

        printf("media = %.2f\n",( notas[0]+notas[1])/2);

        do {
            printf("novo calculo (1-sim 2-nao\n)");
            scanf("%d", &repete);
        } while(repete != 1 && repete !=2);
    } while (repete == 1);

    return 0;
}
