#include <iostream>

int main() {
    int num, i, pos, max=0;

    for(i=1;i<=100;i++){
        scanf("%d", &num);
        if (num>max){
            max = num;
            pos = i;
        }
    }

    printf("%d\n", max);
    printf("%d\n", pos);

    return 0;
}
