#include <iostream>

int main() {
    int n, aux;
    int vet[100];
    int comeco, i, min, posicao, posicao_min;

    scanf("%d", &n);

    //Ler vetor
    for (i=0 ; i<n ; i++){
        scanf("%d", &vet[i]);
    }

    //Ordenar a sequencia
    for (posicao=0 ; posicao<(n-1) ; posicao++){
        min = vet[posicao];
        posicao_min = posicao;

        //Encontra minimo
        for(i=posicao+1 ; i<n ; i++){
            if ( vet[i]<min ){
                min = vet[i];
                posicao_min = i;
            }
        }

        //Troca posicao
        aux = vet[posicao];
        vet[posicao] = min;
        vet[posicao_min] = aux;
    }

    //Escrever vetor
    for (i=0 ; i<n ; i++){
        printf("%d ", vet[i]);
    }

    return 0;
}
