#include <iostream>

int main() {
    int n, i;
    int vet[100];

    scanf("%d", &n);

    //Ler vetor
    for (i=0 ; i<n ; i++){
        scanf("%d", &vet[i]);
    }

    //Escrever vetor
    for (i=n-1 ; i>=0 ; i--){
        printf("%d ", vet[i]);
    }

    return 0;
}
