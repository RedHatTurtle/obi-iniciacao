#include <iostream>

int main() {
    int a, b, c, d, temp;

    scanf("%d %d %d %d", &a, &b, &c, &d);

    if (b < a) {
        temp = a;
        a = b;
        b = temp;
    }

    if (d < c) {
        temp = c;
        c = d;
        d = temp;
    }

    if (c < a) {
        temp = a;
        a = c;
        c = temp;
    }

    if (c < b) {
        temp = b;
        b = c;
        c = temp;
    }

    if (d < c) {
        temp = c;
        c = d;
        d = temp;
    }

    if (c < b) {
        temp = b;
        b = c;
        c = temp;
    }

    printf("%d %d %d %d\n", a, b, c, d);

    return 0;
}
