#include <iostream>

int main () {
    int n, i, j;
    int nota[60];
    int freq;

    scanf("%d", &n);

    for(i=0 ; i<n ; i++){
        scanf("%d", &nota[i]);
    }

    for(i=0 ; i<=10 ; i++){
        freq = 0;
        for (j=0 ; j<n ; j++){
            if (nota[j]==i)
                freq++;
        }
        printf("Nota %d: %d alunos\n", i, freq);
    }
}
