#include<iostream>

int main() {
  int acima,soma=0,N,i;
  float media;
  int notas[1000];

  scanf("%d",&N);

  for (i=0; i<N; i++) {
    scanf("%d", &notas[i]);
    soma = soma + notas[i];
  }

  // calcula a média
  media = soma/N;

  // agora conta quantos estão acima da média
  acima = 0;
  for (i=0; i<N; i++) {
    if (notas[i] > media)
      acima++;
  }

  // imprime o resultado
  printf("%d\n", acima);
  return 0;
}
