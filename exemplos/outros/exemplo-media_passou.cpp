// programa para calcular a m�dia de tr�s notas
#include <iostream>

int p1,p2,p3;
float media;

int main() {
  // L� as tr�s notas
  scanf("%d %d %d", &p1, &p2, &p3);
  // calcula
  media = (p1+p2+p3)/3;
  printf("M�dia: %f\n",media);
  // imprime
  if (media >= 5.0) {
    printf("Aprovado\n");
  }
  else {
    printf("Recupera��o\n");
  }
  return 0;
}
