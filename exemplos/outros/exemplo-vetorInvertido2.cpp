#include <iostream>

int main() {
    int n, i, aux;
    int vet[100];

    scanf("%d", &n);

    //Ler vetor
    for (i=0 ; i<n ; i++){
        scanf("%d", &vet[i]);
    }

    //Inveter o vetor
    for (i=0 ; i<n/2 ; i++){
        aux = vet[i];
        vet[i] = vet[n-1-i];
        vet[n-1-i] = aux;
    }

    //Escrever vetor
    for (i=0 ; i<n ; i++){
        printf("%d ", vet[i]);
    }

    return 0;
}
