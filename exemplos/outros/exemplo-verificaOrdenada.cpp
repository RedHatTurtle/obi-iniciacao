#include <iostream>

int main() {
    int n, i;
    int vet[100];
    int ordenada=0;

    scanf("%d", &n);

    //Ler vetor
    for (i=0 ; i<n ; i++){
        scanf("%d", &vet[i]);
    }

    //Conferir se ordenada
    for (i=0 ; i<n-1 ; i++){
        if (vet[i]<vet[i+1]){
            ordenada++;
        }
    }

    if (ordenada==n-1){
        printf("S\n");
    }else{
        printf("N\n");
    }

    return 0;
}
