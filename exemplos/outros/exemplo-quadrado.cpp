#include <iostream>

int main() {
    float lado1 = 4.56789;
    double lado2 = 4.56789;

    printf("Area (float)  = %f\n", lado1 * lado1);
    printf("Area (double) = %lf\n", lado2 * lado2);
}
