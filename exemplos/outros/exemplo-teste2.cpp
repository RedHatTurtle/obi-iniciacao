#include <iostream>

int main ()
{
    int N, i, nota, soma;
    float media;
    N=2;
    soma=0;
    i=1;
    while (i<=N) {
        scanf("%d", &nota);

        if (nota < 0 || nota > 10) {
            printf("nota invalida\n");
            continue;
        }

        soma= soma+ nota;
        i++;
    }
    media= 1.0*soma/N;
    printf("media = %f\n", media);

    return 0;
}
