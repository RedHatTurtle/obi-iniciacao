#include <iostream>

int main() {
    int n, i, x;
    int freq[2001];

    for(i=1;i<=2000;i++)
        freq[i]=0;

    scanf("%d", &n);

    for(i=1;i<=n;i++){
        scanf("%d", &x);
        freq[x]++;
    }

    for(i=1;i<=2000;i++){
        if (freq[i] != 0)
            printf("%d aparece %d vez(es)\n", i, freq[i]);
    }

    return 0;
}
