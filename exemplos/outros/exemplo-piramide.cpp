#include <iostream>

int main() {
    int i, contador;

    contador = 0;
    while (contador < 8) {
        i = 0;
        while (i < contador) {
            printf(" ");
            i = i + 1;
        }

        i = 0;
        while (i < 8 - contador) {
            printf("o");
            i = i + 1;
        }
        printf("\n");
        contador = contador + 1;
    }
}
