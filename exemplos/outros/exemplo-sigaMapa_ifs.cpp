#include <iostream>

int main() {
    int linhas, colunas;
    int mapa[100][100];
    int i, j, movimentos;

  // L� o n�mero de linhas e o n�mero de colunas do mapa
  scanf("%d %d", &linhas, &colunas);

  // L� o mapa
  for (i=0; i<linhas; i++)
    for (j=0; j<colunas; j++)
      scanf("%d", &mapa[i][j]);

  // Inicia da posicao 0 0
  movimentos = 0;
  i=0; j=0;

  // Conferir se alguma das posi��es ao redor � v�lida
  while(    ( i>0 && mapa[i-1][j]==1 ) || ( i<linhas-1  && mapa[i+1][j]==1 )
         || ( j>0 && mapa[i][j-1]==1 ) || ( j<colunas-1 && mapa[i][j+1]==1 ) ) {
    // Marca essa posi��o como visitada
    mapa[i][j] = 2;

    // Testa se a posi��o abaixo da atual pertence ao tabuleiro
    if (i>0) {
        // Sabendo que a posi��o existe no tabuleiro ent�o podemos conferir se
        // ela � v�lida

        // Testa se a posi��o abaixo � v�lida
        if (mapa[i-1][j]==1){ 
            i = i-1;      // Anda para baixo
            movimentos++; // Soma 1 movimento a mais
        }
    }

    // Testa se a posi��o acima da atual pertence ao tabuleiro
    if (i<linhas-1) {
        // Testa se a posi��o acima � v�lida
        if (mapa[i+1][j]==1){
            i = i+1;      // Anda para cima
            movimentos++; // Soma 1 movimento a mais
        }
    }

    // Testa se a posi��o a esquerda da atual pertence ao tabuleiro
    if (j>0) {
        if (mapa[i][j-1]==1){
            j = j-1;      // Anda para a esquerda
            movimentos++; // Soma 1 movimento a mais
        }
    }

    // Testa se a posi��o a direita da atual pertence ao tabuleiro
    if (j<colunas-1) {
        if (mapa[i][j+1]==1){
            j = j+1;      // Anda para a direita
            movimentos++; // Soma 1 movimento a mais
        }
    }
  }

  // Imprime o resultado
  printf("%d\n", movimentos);
  return 0;
}
