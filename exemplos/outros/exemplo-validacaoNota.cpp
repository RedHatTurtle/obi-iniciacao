#include <iostream>

int main() {
    float nota, sum=0, cont=0;

    while (cont<2){

        scanf("%f", &nota);

        if (nota >= 0 && nota <= 10){
            sum+=nota;
            cont++;
        }else{
            printf("nota invalida\n");
        }
    }

    printf("media = %.2f\n", sum/2.0);

    return 0;
}
