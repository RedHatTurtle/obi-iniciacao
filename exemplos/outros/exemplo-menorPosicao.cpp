#include <iostream>

int main() {
    int num, n, i, pos, min;

    scanf("%d", &n);
    scanf("%d", &min);
    pos=1;

    for(i=2;i<=n;i++){
        scanf("%d", &num);
        if (num<min){
            min = num;
            pos = i;
        }
    }

    printf("Menor valor: %d\n", min);
    printf("Posicao: %d\n", pos-1);

    return 0;
}
