#include <iostream>

int vet[100];
int i, j, N;

int main() {

  // Lê a entrada
  scanf("%d", &N);
  for (i=0; i<N; i++)
    scanf("%d", &vet[i]);

  // verifica; vamos usar dois índices, i e j
  i = 0;
  j = N-1;
  while (i<j) {
    if (vet[i]!=vet[j])
      break;
    i++;
    j--;
  }

  // agora verificamos a condição de parada do while
  if (i<j)
    printf("N\n"); // não é palíndrome
  else
    printf("S\n"); // palíndrome todos as posições foram verificadas (portanto i>=j)
  return 0;
}
