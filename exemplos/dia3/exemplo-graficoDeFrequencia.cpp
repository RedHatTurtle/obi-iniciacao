#include <iostream>

int main() {

    int freq[11]; // notas de 0 a 10
    int i, N, nota;

    scanf("%d",&N);

    // Inicializar vetor de frequência
    for (i=0; i<=10; i++)
        freq[i] = 0;

    // Ler cada nota e atualiza o elemento correspondente no vetor
    for (i=0; i<N; i++) {
        scanf("%d", &nota);
        freq[nota]++; // freq[nota] = freq[nota] + 1
    }

    // Imprimir o resultado
    for (i=0; i<11; i++)
    {
        printf("%2d: ", i);

        for (int j=0 ; j<freq[i] ; j++)
        {
            printf("*");
        }
        printf("\n");
    }

    return 0;
}
