#include <iostream>

int main() {

    int frequenciaNota[11]; // notas de 0 a 10
    int i, N, nota;

    // Ler o número de alunos
    scanf("%d",&N);

    // Zerar o vetor de frequências de notas
    for (i=0; i<=10; i++)
        frequenciaNota[i] = 0;

    // Ler cada nota e atualiza o elemento correspondente no vetor
    for (i=0; i<N; i++) {

        // Ler a nota do aluno "i"
        scanf("%d", &nota);

        // Contabilizar a nota no vetor de frequências de notas
        frequenciaNota[nota]++;
    }

    // Imprimir o resultado
    for (i=0; i<11; i++)
        printf("Nota %d: %d alunos\n", i, frequenciaNota[i]);

    return 0;
}
