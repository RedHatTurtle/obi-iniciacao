#include <iostream>

int main() {

    int tamanho1, tamanho2;
    int sequencia1[100];
    int sequencia2[100];
    int i, j;

    scanf("%d %d", &tamanho1, &tamanho2);

    for (i=0 ; i<tamanho1 ; i++)
        scanf("%d", &sequencia1[i]);

    for (i=0 ; i<tamanho2 ; i++)
        scanf("%d", &sequencia2[i]);

    i = 0;
    j = 0;
    while( i < tamanho1 || j < tamanho2)
    {
        if (sequencia1[i] < sequencia2[j] || j==tamanho2)
        {
            printf("%d ", sequencia1[i]);
            i++;
        }
        else
        {
            printf("%d ", sequencia2[j]);
            j++;
        }
    }
    printf("\n");

    return 0;
}
