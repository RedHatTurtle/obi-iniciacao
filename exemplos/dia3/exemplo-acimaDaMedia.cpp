#include<iostream>

int main() {
    int N, i, notasAcima;
    int notas[60];
    float soma, media;

    scanf("%d", &N);
    soma=0;
    notasAcima=0;

    i=0;
    while (i<N)
    {
        scanf("%d", &notas[i]);
        soma = soma + notas[i];
        i++;  // i = i + 1;
    }
    media = soma/N;
    printf("%f\n", media);

    for(i=0 ; i<N ; i++)
    {
        if (notas[i] > media)
            notasAcima++;
    }
    printf("%d\n", notasAcima);

    return 0;
}
