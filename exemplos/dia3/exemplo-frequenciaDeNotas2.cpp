#include<iostream>

int main() {
    int N, i, frequenciaNota;
    int notas[60];

    // Ler o número de alunos
    scanf("%d", &N);

    // Ler a nota de cada aluno
    i=0;
    while (i<N)
    {
        scanf("%d", &notas[i]);
        i++;
    }

    // Repetir para cada nota
    for (i=0 ; i<=10 ; i++)
    {
        // Primeiro zeramos o contador de frequencia
        frequenciaNota = 0;

        // Agora percorremos o vetor de notas contando quantos
        // alunos tiraram a nota "i"
        for (int j=0 ; j<N ; j++)
        {
            if (notas[j] == i)
                frequenciaNota++;
        }

        if (frequenciaNota > 0)
            printf("Nota %d: %d alunos\n", i, frequenciaNota);
    }

    return 0;
}
