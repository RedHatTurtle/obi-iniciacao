#include <iostream>

int main() {

    int n;
    int elementos[100];

    scanf("%d", &n);

    for (int i=0 ; i<n ; i++)
        scanf("%d", &elementos[i]);

    for (int i=0 ; i<=n/2 ; i++)
    {
        if (elementos[i] != elementos[(n-1)-i])
        {
            printf("N\n");
            break;
        }

        if (i == n/2)
            printf("S\n");
    }
    
    return 0;
}
