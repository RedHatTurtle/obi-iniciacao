#include <iostream>

int n, m; //tamanho do mapa
char mapa[100][100];
int a, b; //posição inicial

int main(){
    int i, j;
    int preso;

    scanf("%d %d %d %d", &n, &m, &a, &b);

    for (i = 0; i < n; i++){
        for (j = 0; j < m; j++){
            scanf(" %c", &mapa[i][j]);
        }
    }

    a--;
    b--;

    preso = 0;
    while(preso == 0){
        if (a == 0 || a == n - 1 || b == 0 || b == m - 1){ //verifica se está na saída da caverna
            //estou na porta da caverna
            break;
        }

        mapa[a][b] = '0';
        if (mapa[a-1][b] == '.'){
            a--;
        }
        else if (mapa[a][b+1] == '.'){
            b++;
        }
        else if (mapa[a+1][b] == '.'){
            a++;
        }
        else if (mapa[a][b-1] == '.'){
            b--;
        }
        else{
            //preso
            preso = 1;
        }
    }

    if (preso){
        printf("Preso!\n");
    }
    else{
        printf("Saiu!\n");
    }
    return 0;
}
