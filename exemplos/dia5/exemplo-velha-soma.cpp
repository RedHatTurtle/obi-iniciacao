#include<iostream>

int main()
{
    int tabuleiro[3][3];
    int somaHorizontal;
    int somaVertical;
    int somaDiagonal1;
    int somaDiagonal2;
    int vencedor = -1;

    // Ler tabuleiro
    for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
            scanf("%d", &tabuleiro[i][j]);

    // Verificar as linhas
    for (int i=0; i<3; i++) {

        somaHorizontal = 0;
        somaVertical   = 0;
        somaDiagonal1  = 0;
        somaDiagonal2  = 0;

        for (int j=0; j<3; j++)
        {
            somaHorizontal = somaHorizontal + tabuleiro[i][j];
            somaVertical   = somaVertical   + tabuleiro[j][i];
            somaDiagonal1  = somaDiagonal1  + tabuleiro[j][j];
            somaDiagonal2  = somaDiagonal2  + tabuleiro[j][2-j];

            if (somaHorizontal == 0 || somaVertical == 0 || somaDiagonal1 == 0 || somaDiagonal2 == 0) {
                vencedor = 0;
                break;
            } else if (somaHorizontal == 3 || somaVertical == 3 || somaDiagonal1 == 3 || somaDiagonal2 == 3) {
                vencedor = 1;
                break;
            }
        }

        if (vencedor == 0 || vencedor == 1)
            break;
    }

    if (vencedor == 0)
        printf("O\n");
    else if (vencedor == 1)
        printf("X\n");
    else
        printf("V\n");

    return 0;
}
