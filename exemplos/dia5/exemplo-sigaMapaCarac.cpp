#include <iostream>

int M,N;
char mapa[12][12];
int i,j,movimentos;

int main() {

  // lê número de linhas e número de colunas do mapa
  scanf("%d %d", &M, &N);
  // cria uma moldura para o mapa
  // molduras horizontais
  for (j=0; j<N+2; j++) {
    mapa[0][j] = 'x';
    mapa[M+1][j] = 'x';
  }
  // molduras verticais
  for (i=0; i<M+2; i++) {
    mapa[i][0] = 'x';
    mapa[i][N+1] = 'x';
  }
  // lê o mapa
  for (i=1; i<=M; i++)
    for (j=1; j<=N; j++)
      scanf(" %c", &mapa[i][j]);
  // inicia da posicao 1 1
  movimentos = 0;
  i=1; j=1;
  while (1) {
    mapa[i][j] = '='; // marca como visitado
    if (mapa[i-1][j]=='*') 
      i = i-1; // nova posição
    else if (mapa[i+1][j]=='*') 
      i = i+1;
    else if (mapa[i][j-1]=='*')
      j = j-1;
    else if (mapa[i][j+1]=='*')
      j = j+1;
    else
      break;
    movimentos++;
  }
  // imprimir o resultado
  for (i=0; i<M+2; i++) {
    for (j=0; j<N+2; j++)
      printf("%c",mapa[i][j]);
    printf("\n");
  }

  printf("%d\n",movimentos);
  return 0;
}
