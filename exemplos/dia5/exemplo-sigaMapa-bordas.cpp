#include <iostream>

int main() {
    int M, N, mapa[12][12];
    int i, j, movimentos;

  // Ler número de linhas e número de colunas do mapa
  scanf("%d %d", &M, &N);

  // Zerar o mapa inicial
  for (i=0; i<12; i++)
    for (j=0; j<12; j++)
      mapa[i][j] = 0;

  // Ler o mapa
  for (i=1; i<=M; i++)
    for (j=1; j<=N; j++)
      scanf("%d", &mapa[i][j]);

  // Inicia da posição [0][0]
  movimentos = 0;
  i=1; j=1;

  while (1==1)
  {
    mapa[i][j] = 2; // Marca como visitado

    if (mapa[i-1][j]==1) // Posição acima da atual
      i = i-1; // Atualizar posição
    else if (mapa[i+1][j]==1) // Posição abaixo da atual
      i = i+1;
    else if (mapa[i][j-1]==1) // Posição à esquerda da atual
      j = j-1;
    else if (mapa[i][j+1]==1) // Posição à direita da atual
      j = j+1;
    else
      break;
    movimentos++;
  }

  // Imprimir o resultado
  printf("%d\n",movimentos);
  return 0;
}
