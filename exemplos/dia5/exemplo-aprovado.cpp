#include <iostream>

int N, i, nota1,nota2,nota3;
float notaAluno[100], mediaTurma;


int main() {

  scanf("%d", &N);
  mediaTurma=0; // inicializa para acumular as médias calculadas
  // lê as notas, calculando e armazenando a média de cada aluno
  for (i=0; i<N; i++) {
    scanf("%d %d %d", &nota1, &nota2, &nota3);
    notaAluno[i] = (nota1 + nota2 + nota3)/3.0;
    mediaTurma = mediaTurma + notaAluno[i];
  }
  mediaTurma = mediaTurma/N;
  // para cada aluno, imprime resultado
  for (i=0; i<N; i++) {
    if (notaAluno[i] > mediaTurma)
      printf("Aprovado\n");
    else
      printf("Reprovado\n");
  }
  return 0;
}
