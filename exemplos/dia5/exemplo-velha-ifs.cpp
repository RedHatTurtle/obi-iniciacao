#include<iostream>

int main()
{
    int tabuleiro[3][3];
    int vencedor = -1;

    // Ler tabuleiro
    for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
            scanf("%d", &tabuleiro[i][j]);

    // Loop por jogador
    for (int jogador=0; jogador<=1; jogador++)
    {
        // Verificar as linhas
        for (int lin=0; lin<3; lin++)
            for (int col=0; col<3; col++)
            {
                if (tabuleiro[lin][col] != jogador)
                    break;
                if (col == 2)
                    vencedor = jogador;
            }

        // Verificar as colunas
        for (int col=0; col<3; col++)
            for (int lin=0; lin<3; lin++)
            {
                if (tabuleiro[lin][col] != jogador)
                    break;
                if (lin == 2)
                    vencedor = jogador;
            }

        // Verificar diagonal principal
        for (int lincol=0; lincol<3; lincol++)
        {
            if (tabuleiro[lincol][lincol] != jogador)
                break;
            if (lincol == 2)
                vencedor = jogador;
        }

        // Verificar diagonal secundária
        for (int lincol=0; lincol<3; lincol++)
        {
            if (tabuleiro[lincol][2-lincol] != jogador)
                break;
            if (lincol == 2)
                vencedor = jogador;
        }

        if (vencedor == jogador)
            break;
    }

    if (vencedor == 0)
        printf("O\n");
    else if (vencedor == 1)
        printf("X\n");
    else
        printf("V\n");

    return 0;
}
