#include <iostream>

int main() {
    int vetor[100];
    int i;
    int maior;
    int posicao;

    for( i=0 ; i<100 ; i++ ) {
        scanf("%d", &vetor[i]);

        // Se for o primeiro numero usamos ele como primeiro chute
        if (i==0) {
            maior = vetor[i];
            posicao = i+1;
        }
        else {
            // Se não for o primeiro então comparamos com o maior até agora
            if (vetor[i] > maior){
                maior = vetor[i];
                // A posição comeca em 1 enquanto o índice começa em 0 então somamos 1
                posicao = i+1;
            }
        }
    }

    // Imprimir os resultados
    printf("%d\n", maior);
    printf("%d\n", posicao);

    return 0;
}
