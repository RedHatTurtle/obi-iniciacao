#include <iostream>

int main() {

    float nota1 = -1;
    float nota2 = -1;
    float media;

    // Ler a primeira nota até encontrar uma nota válida
    while (nota1 < 0 || nota1 > 10) {
        scanf("%f", &nota1);

        // Se a nota for inválida imprimir a mensagem de erro
        if (nota1 < 0 || nota1 > 10) {
            printf("nota invalida\n");
        }
    }

    // Alternativa
    // Ler a segunda nota indefinidamente
    while (true) {
        scanf("%f", &nota2);

        if (nota2 >= 0 && nota2 <= 10) {
            // Se a nota for válida sair do while
            break;
        } else {
            // Se não imprimir a mensagem de erro
            printf("nota invalida\n");
        }
    }

    // Agora que temos duas notas válidas podemos calcular a média
    media = (nota1 + nota2)/2.0;

    printf("media = %.2f\n", media);

    return 0;
}
