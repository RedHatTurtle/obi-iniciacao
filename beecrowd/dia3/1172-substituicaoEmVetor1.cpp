#include <iostream>

int main() {

    int vetor[10];
    int i;

    // Ler os 10 elementos do vetor e verificar se cada um deles é positivo
    for (i=0 ; i<10 ; i++) {
        scanf("%d", &vetor[i]);

        // Se o valor não for positivo substituir por 1
        if (vetor[i] <= 0) {
            vetor[i] = 1;
        }
    }

    // Imprimir o vetor alterado
    for (i=0 ; i<10 ; i++) {
        printf("X[%d] = %d\n", i, vetor[i]);
    }

    return 0;
}
