#include <iostream>
int main()
{
    int n;
    int quantidade;
    char tipoAnimal;
    int totalCoelhos, totalRatos, totalSapos;

    scanf("%d", &n);
    totalCoelhos = 0;
    totalRatos = 0;
    totalSapos = 0;

    for(int i=0; i<n ; i++){
        scanf("%d %c", &quantidade, &tipoAnimal);

        if (tipoAnimal == 'C')
            totalCoelhos = totalCoelhos + quantidade;
        else if (tipoAnimal == 'R')
            totalRatos = totalRatos + quantidade;
        else if (tipoAnimal == 'S')
            totalSapos = totalSapos + quantidade;
    }

    printf("Total: %d cobaias\n", totalCoelhos+totalRatos+totalSapos);
    printf("Total de coelhos: %d\n", totalCoelhos);
    printf("Total de ratos: %d\n", totalRatos);
    printf("Total de sapos: %d\n", totalSapos);
    printf("Percentual de coelhos: %.2f %%\n", (100.0*totalCoelhos)/(totalCoelhos+totalRatos+totalSapos));
    printf("Percentual de ratos: %.2f %%\n", (100.0*totalRatos)/(totalCoelhos+totalRatos+totalSapos));
    printf("Percentual de sapos: %.2f %%\n", (100.0*totalSapos)/(totalCoelhos+totalRatos+totalSapos));

    return 0;
}
