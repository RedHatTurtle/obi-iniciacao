#include <iostream>

int main() {

    int x=1;
    float nota, nota1, nota2;

    while (x==1)
    {
        nota1 = -1.0;
        nota2 = -1.0;

        while (nota2 < 0)
        {
            scanf("%f", &nota);

            if (nota >= 0.0 && nota <= 10.0)
            {
                // Salvar nota válida
                if (nota1 < 0)
                    nota1 = nota;
                else
                    nota2 = nota;
            }
            else
            {
                printf("nota invalida\n");
            }
        }

        printf("media = %.2f\n", (nota1+nota2)/2.0);

        printf("novo calculo (1-sim 2-nao)\n");
        scanf("%d", &x);
        while ( x != 1 && x != 2)
        {
            printf("novo calculo (1-sim 2-nao)\n");
            scanf("%d", &x);
        }
    }

    return 0;
}
