#include <iostream>

int main() {
    int n, x;

    scanf("%d", &n);

    for (int i=0 ; i<n ; i++)
    {
        scanf("%d", &x);

        for (int div=2 ; div<=x ; div++)
        {
            if (div == x)
            {
                printf("%d eh primo\n", x);
            }
            else if (x%div == 0)
            {
                printf("%d nao eh primo\n", x);
                break;
            }
        }
    }

    return 0;
}
