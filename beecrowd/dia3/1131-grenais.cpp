#include <iostream>

int main() {
    int x = 1;
    int gols_inter, gols_gremio;
    int vInter, vGremio, empates;

    vInter = 0;
    vGremio = 0;
    empates = 0;

    while (x==1)
    {
        scanf("%d %d", &gols_inter, &gols_gremio);

        if (gols_inter > gols_gremio)
            vInter++;
        else if (gols_gremio > gols_inter)
            vGremio++;
        else
            empates++;

        printf("Novo grenal (1-sim 2-nao)\n");
        scanf("%d", &x);
        while (x != 1 && x!= 2)
        {
            printf("Novo grenal (1-sim 2-nao)\n");
            scanf("%d", &x);
        }
    }

    printf("%d grenais\n", vInter+vGremio+empates);
    printf("Inter:%d\n", vInter);
    printf("Gremio:%d\n", vGremio);
    printf("Empates:%d\n", empates);

    if (vInter > vGremio)
        printf("Inter venceu mais\n");
    else if (vGremio > vInter)
        printf("Gremio venceu mais\n");
    else
        printf("Nao houve vencedor\n");

    return 0;
}
