#include <iostream>

int main() {
    int i;
    float vetor[100];

    // Ler todo o vetor
    for(i=0;i<100;i++)
        scanf("%f", &vetor[i]);

    // Agora percorremos o vetor
    for(i=0;i<100;i++) {
        // Procurando valores menores ou iguais a 10
        if (vetor[i] <= 10) {
            // Para imprimirmos
            printf("A[%d] = %.1f\n", i, vetor[i]);
        }
    }

    return 0;
}
