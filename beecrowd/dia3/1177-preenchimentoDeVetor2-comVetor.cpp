#include <iostream>

int main() {
    int t, i;
    int vetor[1000];

    // Ler o valor T
    scanf("%d", &t);

    // Preencher o vetor com numeros de 0 até t-1
    for(i=0;i<1000;i++) {
        vetor[i] = i%t;
    }

    // Imprimir o vetor
    for(i=0;i<1000;i++) {
        printf("N[%d] = %d\n", i, vetor[i]);
    }

    return 0;
}
