#include <iostream>

int main() {
    int ini, fim;
    int aux;
    int soma=0;

    scanf("%d %d", &ini, &fim);

    if (ini > fim)
    {
        aux = ini;
        ini = fim;
        fim = aux;
    }

    for (int cont=ini ; cont<=fim ; cont++)
    {
        if (cont%13 != 0)
            soma += cont; // soma = soma + cont
    }

    printf("%d\n", soma);

    return 0;
}
