#include <iostream>

int main() {

    int numero;
    int divisor;

    // Ler o número do qual queremos os divisores
    scanf("%d", &numero);

    // Iterar por todos os numero de 1 a "número" procurando divisores
    for( divisor=1 ; divisor <= numero ; divisor++ ) {
        // Se a divisão tiver resto zero então o número é um divisor
        if (numero%divisor == 0) {
            // Imprimir o divisor
            printf("%d\n", divisor);
        }
    }

    return 0;
}
