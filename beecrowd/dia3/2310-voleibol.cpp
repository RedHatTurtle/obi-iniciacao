#include <iostream>

int main()
{
    int n;
    char nome[20];
    int  s,  b,  a;
    int s_tot, b_tot, a_tot;
    int s_sus, b_sus, a_sus;

    s_tot = 0;
    b_tot = 0;
    a_tot = 0;
    s_sus = 0;
    b_sus = 0;
    a_sus = 0;

    scanf("%d", &n);

    for (int jogador=1 ; jogador <= n ; jogador ++)
    {
        scanf("%c", &nome[0]);
        for (int i = 0; i<20 ; i++){
            scanf("%c", &nome[i]);
            if (nome[i] == '\n')
                break;
        }

        scanf("%d %d %d", &s, &b, &a);

        s_tot  += s;
        b_tot  += b;
        a_tot  += a;

        scanf("%d %d %d", &s, &b, &a);

        s_sus  += s;
        b_sus  += b;
        a_sus  += a;
    }

    printf("Pontos de Saque: %.2f %%.\n",    100.0*s_sus/s_tot);
    printf("Pontos de Bloqueio: %.2f %%.\n", 100.0*b_sus/b_tot);
    printf("Pontos de Ataque: %.2f %%.\n",   100.0*a_sus/a_tot);

    return 0;
}
