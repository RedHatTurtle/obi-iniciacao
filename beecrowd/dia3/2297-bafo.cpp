#include <iostream>

int main() {
    int r;
    int a, b;
    int tot_a, tot_b;
    int n=1;

    scanf("%d", &r);

    while (r > 0)
    {
        tot_a = 0;
        tot_b = 0;

        for (int i=0; i<r ; i++)
        {
            scanf("%d %d", &a, &b);
            tot_a += a;
            tot_b += b;
        }
        printf("Teste %d\n", n);
        if (tot_a > tot_b)
            printf("Aldo\n");
        else
            printf("Beto\n");
        printf("\n");
        n++;

        scanf("%d", &r);
    }

    return 0;
}
