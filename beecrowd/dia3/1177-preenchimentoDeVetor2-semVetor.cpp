#include <iostream>

int main() {
    int t, i;

    // Ler o valor T
    scanf("%d", &t);

    // Imprimir o vetor
    for(i=0;i<1000;i++){
        printf("N[%d] = %d\n", i, i%t);
    }

    return 0;
}
