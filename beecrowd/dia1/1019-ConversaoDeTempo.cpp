#include <iostream>

int main() {
    int t;
    int h, m, s;

    scanf("%d", &t);

    h =  t/(60*60);
    m = (t%3600)/60;
    s =  t%60;

    printf("%d:%d:%d\n", h, m, s);

    return 0;
}
