#include <iostream>

int main () {

    int n;
    int horas, minutos, segundos;

    scanf("%d", &n);

    //conveter em horas:miutos:segundos
    horas = n/(60*60);

    minutos = (n % 3600)/60;

    segundos = n % 60;

    printf("%2d:%2d:%2d\n", horas, minutos,segundos);

}
