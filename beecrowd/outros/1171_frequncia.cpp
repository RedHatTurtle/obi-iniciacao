#include <iostream>

int main(){
    int n, i, a;
    int x[2000];

    scanf("%d", &n);

    for( i=0 ; i<2000 ; i++){
        x[i]=0;
    }

    for (i=0 ; i<n ; i++){
        scanf("%d", &a);
        x[a-1]++;
    }

    for (i=0 ; i<2000 ; i++){
        if(x[i]>0){
            printf("%d aparece %d vez(es)\n", i+1, x[i]);
        }
    }

    return 0;
}


