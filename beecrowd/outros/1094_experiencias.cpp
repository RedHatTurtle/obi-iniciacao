#include <iostream>

int main() {
    int n, i;
    int quantia;
    char tipo;
    int cont_c=0, cont_r=0, cont_s=0;

    scanf("%d", &n);

    for (i=0 ; i<n ; i++){
        scanf("%d %c", &quantia, &tipo);
        if (tipo == 'C'){
            cont_c = cont_c + quantia;
        }else if (tipo == 'R'){
            cont_r = cont_r + quantia;
        }else if (tipo == 'S'){
            cont_s = cont_s + quantia;
        }
    }

    printf("Total: %d cobaias\n", cont_c+cont_r+cont_s);
    printf("Total de coelhos: %d\n", cont_c);
    printf("Total de ratos: %d\n", cont_r);
    printf("Total de sapos: %d\n", cont_s);
    printf("Percentual de coelhos: %.2f %%\n", (cont_c*100.0)/(cont_c+cont_r+cont_s) );
    printf("Percentual de ratos: %.2f %%\n", (cont_r*100.0)/(cont_c+cont_r+cont_s));
    printf("Percentual de sapos: %.2f %%\n", (cont_s*100.0)/(cont_c+cont_r+cont_s));

    return 0;
}
