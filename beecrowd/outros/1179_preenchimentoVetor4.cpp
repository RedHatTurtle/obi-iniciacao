#include <iostream>

int main(){
    int pares[5], impares[5];
    int i, x, cp=0, ci=0;

    for (i=0 ; i<15 ; i++){
        scanf("%d", &x);


        if(x%2==0){
            pares[cp]=x;
            cp++;
        }
        else{
            impares[ci]=x;
            ci++;
        }

        if (cp==5){
            for (cp=0 ; cp<5 ; cp++){
                printf("par[%d] = %d\n", cp, pares[cp]);
            }
            cp=0;
        }
        if (ci==5){
            for (ci=0 ; ci<5 ; ci++){
                printf("impar[%d] = %d\n", ci, impares[ci]);
            }
            ci=0;
        }
    }

    for (i=0 ; i<ci ; i++){
        printf("impar[%d] = %d\n", i, impares[i]);
    }

    for (i=0 ; i<cp ; i++){
        printf("par[%d] = %d\n", i, pares[i]);
    }

    return 0;
}


