#include <iostream>

int main () {
    float renda, imposto;

    scanf("%f", &renda);

    if (renda<=2000.0)
        printf("Isento\n");
    else if ( renda <= 3000.0){
        imposto = (renda-2000.0)*0.08;
        printf("R$ %.2f\n", imposto);
    }
    else if ( renda <= 4500.0){
        imposto = (renda-3000.0)*0.18;
        imposto = imposto + 80.0;
        printf("R$ %.2f\n", imposto);
    }
    else {
        imposto = (renda-4500.0)*0.28;
        imposto = imposto + 80.0 + 270.0;
        printf("R$ %.2f\n", imposto);
    }

    return 0;
}
