#include <iostream>

int main() {
    int n;
    int a, b;
    int i;
    int tot_a, tot_b;
    int t=1;

    //Ler numero de rodadas
    scanf("%d", &n);

    while(n>0){
        tot_a = 0;
        tot_b = 0;

        //Ler cada rodada
        for (i=1 ; i<=n ; i++){
            scanf("%d %d", &a, &b);
            //Calcular resultado
            tot_a = tot_a + a;
            tot_b = tot_b + b;
        }

        printf("Teste %d\n", t);
        //Imprimir resposta
        if (tot_a > tot_b){
            printf("Aldo\n");
        }else{
            printf("Beto\n");
        }
        printf("\n");

        //Ler numero de rodadas
        scanf("%d", &n);
        t++;
    }

    return 0;
}
