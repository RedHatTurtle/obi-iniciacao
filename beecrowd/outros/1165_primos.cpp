#include <iostream>

int main() {
    int n, i;
    int x, div;

    //Le quantidade de testes
    scanf("%d", &n);


    for (i=0 ; i<n ; i++){
        //Le teste
        scanf("%d", &x);

        //Testa se primo
        for(div=2 ; div*div<=x ; div++){
            if (x%div==0){
                printf("%d nao eh primo\n", x);
                break;
            }
        }

        if (div*div>x){
            printf("%d eh primo\n", x);
        }
    }

    return 0;
}
