#include <iostream>

int main() {
    int i, n;

    scanf("%d", &n);

    for(i=0;i<1000;i++){
        printf("N[%d] = %d\n", i, i%n);
    }

    return 0;
}
