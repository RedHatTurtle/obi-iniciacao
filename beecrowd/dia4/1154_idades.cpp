#include <iostream>

int main() {
    int idade=0;
    int soma=0;
    int idosos=0;

    while(idade>=0){
        scanf("%d", &idade);
        soma = soma + idade;
        idosos++;
    }

    printf("%.2f\n", (soma-idade)/(idosos-1.0));

    return 0;
}
