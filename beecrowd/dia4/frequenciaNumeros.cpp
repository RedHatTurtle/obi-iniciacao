#include <iostream>

int main() {
    int n, i, x;
    int freq[2000];

    // Ler quantidade de numero na lista
    scanf("%d", &n);

    // Zerar vetor de frequencia dos números
    for(i=0 ; i<2000 ; i++)
        freq[i]=0;

    // Ler a lista de numero e contabilizar as apariçÕes de cada número
    for(i=0 ; i<n ; i++){
        scanf("%d", &x);
        freq[x-1]++;
    }

    // Imprimir Saída
    for(i=0 ; i<2000 ; i++){
        if (freq[i] != 0)
            printf("%d aparece %d vez(es)\n", i+1, freq[i]);
    }

    return 0;
}

//    i =  0  1  2  3  4  5  6  7  8  9 10 11 12 13  ...
// freq = [0, 0, 0, 1, 0, 0, 0, 2, 0, 3, 0, 0, 0, 0, ...
