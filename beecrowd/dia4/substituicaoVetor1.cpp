#include <iostream>

int main() {
    int i;
    int vec[10];

    for(i=0;i<10;i++)
        scanf("%d", &vec[i]);

    for(i=0;i<10;i++){
        if (vec[i] <= 0)
            vec[i]=1;
        printf("X[%d] = %d\n", i, vec[i]);
    }

    return 0;
}
