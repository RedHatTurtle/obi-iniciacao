#include <iostream>

int main() {
    int n, dist;
    int proxima_parada, ultima_parada;
    char res='S';

    // Ler número de postos de parada e distância intermediária máxima
    scanf("%d %d", &n, &dist);

    // Ler a localização do primeiro posto de parada
    scanf("%d", &ultima_parada);

    // Ler as próximas paradas e verificar a distância
    for(int i=1 ; i<=n ; i++){
        scanf("%d", &proxima_parada);

        if (proxima_parada - ultima_parada > dist)
            res = 'N';

        // Atualizar localização da última parada
        ultima_parada = proxima_parada;
    }

    // Verificar distância da última parada até o final da prova
    if (42195-ultima_parada > dist)
        res = 'N';

    // Imprimir resposta
    printf("%c\n", res);

    return 0;
}
