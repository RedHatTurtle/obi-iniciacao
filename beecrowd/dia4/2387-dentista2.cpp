#include <iostream>

int main() {
    int n, i;
    int ini[10000];
    int fim[10000];
    int consultas, hora;

    scanf("%d", &n);

    for (i=0 ; i<n ; i++){
        scanf("%d %d", &ini[i], &fim[i]);
    }

    consultas = 1;
    hora = ini[n-1];
    for(i=n-2 ; i>=0 ; i--){
        if(fim[i]<=hora){
            consultas++;
            hora = ini[i];
        }
    }

    printf("%d\n", consultas);

    return 0;
}
