#include <iostream>

int main() {
    int n, i;
    int inicio, fim;
    int consultas = 0;
    int ultimoFim = -1;

    // Ler o numero de consultas possíveis
    scanf("%d", &n);

    for (i=0 ; i<n ; i++){
        // Ler os horarios de inicio e fim da consulta
        scanf("%d %d", &inicio, &fim);

        if (inicio >= ultimoFim) {
            // Se a consulta lida inicia ao mesmo tempo ou depois do fim da
            // última consulta então ela pode ser agendada
            consultas++;

            // E o horário de fim da última consulta é atualizado
            ultimoFim = fim;
        } else if (fim < ultimoFim) {
            // Se o inicio da consulta não for compatível com o final da última
            // consulta mas essa consulta terminar mais cedo que a agendada
            // anteriormente então podemos trocar uma pela outra já que essa
            // deixa mais tempo livre até o final do dia
            ultimoFim = fim;
        }
    }

    printf("%d\n", consultas);

    return 0;
}
