#include <iostream>

int main() {
    int i, j, x, np, ni;
    int par[5], impar[5];

    np=0;
    ni=0;

    for(i=0 ; i<15 ; i++){
        scanf("%d", &x);
        if (x%2==0){
            par[np]=x;
            np++;
        }else{
            impar[ni]=x;
            ni++;
        }

        if (np==5){
            for(j=0 ; j<np ; j++)
                printf("par[%d] = %d\n", j, par[j]);
            np=0;
        }
        if (ni==5){
            for(j=0 ; j<ni ; j++)
                printf("impar[%d] = %d\n", j, impar[j]);
            ni=0;
        }
    }
    for(j=0 ; j<ni ; j++)
        printf("impar[%d] = %d\n", j, impar[j]);
    for(j=0 ; j<np ; j++)
        printf("par[%d] = %d\n", j, par[j]);

    return 0;
}
