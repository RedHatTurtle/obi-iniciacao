#include<iostream>

int main()
{
    int aeroportos, voos;
    int x, y;
    int trafego[100];
    int maximo;
    int cont=1;

    // Ler quantidade de aeroportos e voos
    scanf("%d %d", &aeroportos, &voos);

    while(aeroportos!=0 && voos!=0)
    {
        // Zerar contador de voos de cada aeroporto
        for (int i=0 ; i<100 ; i++)
            trafego[i] = 0;

        // Zerar maximo de voos em um aeroporto
        maximo = 0;

        // Ler todos os voos e contabilizar cada voo nos aeroportos de origem e
        // destino
        for (int i=0 ; i<voos ; i++)
        {
            scanf("%d %d", &x, &y);

            trafego[x-1]++;
            trafego[y-1]++;

            if (maximo < trafego[x-1])
                maximo = trafego[x-1];

            if (maximo < trafego[y-1])
                maximo = trafego[y-1];
        }

        // Imprimir o índice dos aeroportos com tráfego máximo
        printf("Teste %d\n", cont);
        for (int i=0; i<aeroportos ; i++)
            if (trafego[i] == maximo)
                printf("%d ", i+1);
        printf("\n\n");
        cont++;

        // Ler quantidade de aeroportos e voos do teste novo
        scanf("%d %d", &aeroportos, &voos);
    }

    return 0;
}
