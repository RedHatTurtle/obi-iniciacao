#include <iostream>
int main()
{
    int cv, ce, cs;
    int fv, fe, fs;

    scanf("%d %d %d %d %d %d", &cv, &ce, &cs, &fv, &fe, &fs);

    if (3*cv+ce > 3*fv+fe)
        printf("C\n");
    else if (3*cv+ce < 3*fv+fe)
        printf("F\n");
    else if (cs > fs)
        printf("C\n");
    else if (cs < fs)
        printf("F\n");
    else
        printf("=\n");

    return 0;
}
