#include <iostream>
int main()
{
    int n, x, dentro, fora;

    scanf("%d", &n);
    dentro = 0;
    fora = 0;

    for (int cont=1 ; cont <= n ; cont++)
    {
        scanf("%d", &x);
        if (x>=10 && x<=20)
            dentro++;
        else
            fora++;
    }

    printf("%d in\n", dentro);
    printf("%d out\n", fora);

    return 0;
}
