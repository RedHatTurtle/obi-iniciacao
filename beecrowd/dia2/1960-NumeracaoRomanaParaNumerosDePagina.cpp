#include <iostream>
int main()
{
    int num;
    int centena, dezena, unidade;

    scanf("%d", &num);

    centena = num/100;
    dezena  = (num%100)/10;
    unidade = num%10;

    // M = 1000
    // D =  500
    // C =  100
    // L =   50
    // X =   10
    // V =    5
    // I =    1

    if (centena == 9)
        printf("CM");
    else if (centena == 8)
        printf("DCCC");
    else if (centena == 7)
        printf("DCC");
    else if (centena == 6)
        printf("DC");
    else if (centena == 5)
        printf("D");
    else if (centena == 4)
        printf("CD");
    else if (centena == 3)
        printf("CCC");
    else if (centena == 2)
        printf("CC");
    else if (centena == 1)
        printf("C");

    if (dezena == 9)
        printf("XC");
    else if (dezena == 8)
        printf("LXXX");
    else if (dezena == 7)
        printf("LXX");
    else if (dezena == 6)
        printf("LX");
    else if (dezena == 5)
        printf("L");
    else if (dezena == 4)
        printf("XL");
    else if (dezena == 3)
        printf("XXX");
    else if (dezena == 2)
        printf("XX");
    else if (dezena == 1)
        printf("X");

    if (unidade == 9)
        printf("IX");
    else if (unidade == 8)
        printf("VIII");
    else if (unidade == 7)
        printf("VII");
    else if (unidade == 6)
        printf("VI");
    else if (unidade == 5)
        printf("V");
    else if (unidade == 4)
        printf("IV");
    else if (unidade == 3)
        printf("III");
    else if (unidade == 2)
        printf("II");
    else if (unidade == 1)
        printf("I");

    printf("\n");

    return 0;
}
