#include <iostream>
int main()
{
    double a, b, c;
    double aux;

    // Ler os tamanhos dos lados
    scanf("%lf %lf %lf", &a, &b, &c);

    // Para usar as fórmulas do enunciado precisamos descobrir o maior lado

    // Se "B" for maior que "A" trocar "B" com "A"
    if (a<b){
        aux = a;
        a = b;
        b = aux;
    }
    // Se "C" for maior que "A" trocar "C" com "A"
    if (a<c){
        aux = a;
        a = c;
        c = aux;
    }

    // Primeiro checamos se os lados formam um triângulo, se não formar podemos pular todo o resto
    if (a>=b+c)
        printf("NAO FORMA TRIANGULO\n");
    else {
        // Caso forme um triangulo fazemos os outros testes

        // Primeiro testamos se o triangulo é retângulo, acutângulo ou obtusângulo
        if (a*a == b*b + c*c)
            printf("TRIANGULO RETANGULO\n");
        else if (a*a > b*b + c*c)
            printf("TRIANGULO OBTUSANGULO\n");
        else if (a*a < b*b + c*c)
            printf("TRIANGULO ACUTANGULO\n");

        // Depois testamos se existem lados iguais
        if (a == b && b == c) {
            // Se todos os 3 lados forem iguais o triângulo é equilátero
            printf("TRIANGULO EQUILATERO\n");
        } else if (a == b || b == c || a == c) {
            // Se os 3 lados não forem todos iguais mas 2 deles forem então o triângulo é isosceles
            printf("TRIANGULO ISOSCELES\n");
        }
    }

    return 0;
}
