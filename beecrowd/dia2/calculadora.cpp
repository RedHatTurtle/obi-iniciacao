#include <iostream>
int main()
{
    long int num1, num2;
    char operacao;

    scanf("%ld%c%ld", &num1, &operacao, &num2);

    if (operacao == '+')
        printf("%ld\n", num1+num2);
    else if (operacao == '-')
        printf("%ld\n", num1-num2);
    else if (operacao == '*')
        printf("%ld\n", num1*num2);
    else if (operacao == '/')
        printf("%ld\n", num1/num2);
    else
        printf("Operacao invalida\n");

    return 0;
}
