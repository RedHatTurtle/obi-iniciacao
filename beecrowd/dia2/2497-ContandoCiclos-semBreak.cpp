#include <iostream>
int main()
{
    int num, cont;

    cont = 0;

    scanf("%d", &num);
    while (num != -1)
    {
        cont++;
        printf("Experiment %d: %d full cycle(s)\n", cont, num/2 );

        scanf("%d", &num);
    }

    return 0;
}
