#include <iostream>
int main()
{
    int distanciaViagem, distanciaPedagio;
    int custoPorKM, custoPedagio;

    scanf("%d %d", &distanciaViagem, &distanciaPedagio);
    scanf("%d %d", &custoPorKM, &custoPedagio);

    printf("%d\n", distanciaViagem*custoPorKM
                  +(distanciaViagem/distanciaPedagio)*custoPedagio);

    return 0;
}
