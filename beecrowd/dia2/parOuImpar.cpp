#include <iostream>
int main()
{
    int n, x;

    scanf("%d", &n);

    for (int cont=1 ; cont <= n ; cont++)
    {
        scanf("%d", &x);
        if (x==0)
            printf("NULL\n");
        else if (x%2 == 0)
            printf("EVEN ");
        else
            printf("ODD ");

        if (x>0)
            printf("POSITIVE\n");
        else if (x<0)
            printf("NEGATIVE\n");
    }

    return 0;
}
